package require snit

snit::type RedisClient {
  
  variable client ""

  variable subscribed {}

  option -host -default "127.0.0.1" -readonly yes
  option -port -default "6379" -readonly yes

  constructor { args } {
    $self configurelist $args
    set client [ redis $options(-host) $options(-port) 1 ]
  }

  destructor {
    if { $client ne "" } {
      $client close
    }
  }

  method __consume_subscribe_messages { _type channels } {
    set numsub -1
    set counts {}
    
    if { [ llength $channels ] } {
      for {set i [llength $channels]} {$i > 0} {incr i -1} {
        set msg [$client read]
        #assert_equal $_type [lindex $msg 0]
        
        # when receiving subscribe messages the channels names
        # are ordered. when receiving unsubscribe messages
        # they are unordered
        set idx [lsearch -exact $channels [lindex $msg 1]]
        set channels [lreplace $channels $idx $idx]
        
        # aggregate the subscription count to return to the caller
        lappend counts [lindex $msg 2]
      }
      
      # we should have received messages for channels
      #assert {[llength $channels] == 0}
      return $counts
    } elseif { $_type eq "unsubscribe" } {
      $client PING
      set answer ""
      set replyList {}
      while { $answer ne "PONG" } {
        set answer [ $client read ]
        lappend replyList $answer
      }
    }
  }

  method set { key value } {
    $client set $key $value
    return [ $client read ]
  }

  method get { key } {
    $client get $key
    return [ $client read ]
  }

  method del { key } {
    $client del $key
    return [ $client read ]
  }

  method publish { chan message } {
    $client publish $chan $message
    return [ $client read ]
  }

  method subscribe { channels } {
    #$client subscribe {*}$channels
    eval $client subscribe $channels
    $self __consume_subscribe_messages subscribe $channels
  }

  method unsubscribe {{channels {}}} {
    #$client unsubscribe {*}$channels
    eval $client unsubscribe $channels
    $self __consume_subscribe_messages unsubscribe $channels
  }
  
  method psubscribe {channels} {
    #$client psubscribe {*}$channels
    eval $client psubscribe $channels
    $self __consume_subscribe_messages $client psubscribe $channels
  }
  
  method punsubscribe {{channels {}}} {
    #$client punsubscribe {*}$channels
    eval $client punsubscribe $channels
    $self __consume_subscribe_messages $client punsubscribe $channels
  }

  method subscribe_wait_unsubscribe { channels } {
    $self subscribe $channels
    
    array set fullReply {}

    set leftChannel [ llength $channels ]
    foreach chan $channels {
      set waiting($chan) 1
    } 

    while { $leftChannel } {
      foreach { _type chan body } [ $client read ] break
      if { $_type eq "message" } {
        if { [ info exists waiting($chan) ] } {
          # only last publish from channel is reported
          set fullReply($chan) $body
          if { $waiting($chan) } {
            incr leftChannel -1
            set waiting($chan) 0
          }
        } else {
          $self unsubscribe $channels
          error "subscribe_wait_unsubscribe: unexpected reply from channel = $channel"
        }
      } else {
        $self unsubscribe $channels
        error "subscribe_wait_unsubscribe: unexpected reply type = $_type"
      }
    }
    $self unsubscribe $channels
    return [ array get fullReply ]
  }
}

package provide RedisClient 0.1
