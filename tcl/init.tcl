set _cd_ [ file normalize [ file dir [ info script ] ] ]
lappend auto_path [ file join $_cd_ lib ]

package require RedisClient

source [ file join $_cd_ "utils.tcl" ]
